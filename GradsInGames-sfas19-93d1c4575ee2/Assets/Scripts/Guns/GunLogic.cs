﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunLogic : MonoBehaviour
{
    public int m_GunOwnerID;

    // The Bullet Prefab
    [SerializeField]
    protected GameObject m_BulletPrefab;

    // The Explosive Bullet Prefab
    [SerializeField]
    GameObject m_ExplosiveBulletPrefab;

    // The Bullet Spawn Point
    public Transform m_BulletSpawnPoint;

    [SerializeField]
    protected float m_ShotCooldown = 0f;

    [SerializeField]
    protected float m_FireRate = 0.3f;

    protected bool m_CanShoot = true;

    // VFX
    [SerializeField]
    protected ParticleSystem m_Flare;

    [SerializeField]
    protected ParticleSystem m_Smoke;

    [SerializeField]
    protected ParticleSystem m_Sparks;

    // SFX
    [SerializeField]
    protected AudioClip m_BulletShot;

    [SerializeField]
    protected AudioClip m_GrenadeLaunched;

    // The AudioSource to play Sounds for this object
    protected AudioSource m_AudioSource;

    public float m_Range = 20f;
    public int m_BulletAmmo = 100;
    public int m_GrenadeAmmo = 5;



    // Use this for initialization
    void Start ()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!m_CanShoot)
        {
            m_ShotCooldown += Time.deltaTime;
            if (m_ShotCooldown > m_FireRate)
            {
                m_ShotCooldown = 0;
                m_CanShoot = true;
            }
        }
    }

    public bool CanIShoot()
    {
        return m_CanShoot;
    }

    public void Shooting()
    {      

            if (Input.GetButton("Fire1") && m_BulletAmmo > 0)
            {
                Fire();
             
            }
            else if (Input.GetButtonDown("Fire2") && m_GrenadeAmmo > 0)
            {
                FireGrenade();
                m_CanShoot = false;
            }
    }


    virtual public void Fire()
    {
        if (m_CanShoot)
        {
            m_CanShoot = false;
            if (m_BulletPrefab)
            {
                // Reduce the Ammo count
                --m_BulletAmmo;

                // Create the Projectile from the Bullet Prefab
                
                var bullet = Instantiate(m_BulletPrefab, m_BulletSpawnPoint.position, m_BulletSpawnPoint.rotation * m_BulletPrefab.transform.rotation);
                bullet.GetComponent<BulletLogic>().m_BulletOwnerID = m_GunOwnerID;

                // Play Particle Effects
                PlayGunVFX();

                // Play Sound effect
                if (m_AudioSource && m_BulletShot)
                {
                    m_AudioSource.PlayOneShot(m_BulletShot);
                }          
            }
        }
      
    }

    public bool IsGunFiring()
    {
        return m_AudioSource.isPlaying;
    }

    void FireGrenade()
    {
        if (m_CanShoot)
        {
            m_CanShoot = false;
            if (m_ExplosiveBulletPrefab)
            {
                // Reduce the Ammo count
                --m_GrenadeAmmo;

                // Create the Projectile from the Explosive Bullet Prefab
                Instantiate(m_ExplosiveBulletPrefab, m_BulletSpawnPoint.position, transform.rotation * m_ExplosiveBulletPrefab.transform.rotation);

                // Play Particle Effects
                PlayGunVFX();

                // Play Sound effect
                if (m_AudioSource && m_GrenadeLaunched)
                {
                    m_AudioSource.PlayOneShot(m_GrenadeLaunched);
                }

            }
        }
        
    }

    protected void PlayGunVFX()
    {
        if (m_Flare)
        {
            m_Flare.Play();
        }

        if (m_Sparks)
        {
            m_Sparks.Play();
        }

        if (m_Smoke)
        {
            m_Smoke.Play();
        }
    }

    public void AddAmmo(int bullets, int grenades)
    {
        m_BulletAmmo += bullets;
        m_GrenadeAmmo += grenades;
        gameObject.GetComponent<PlayerController>().UpdateUI();
    }
}
