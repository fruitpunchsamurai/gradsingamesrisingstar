﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyAIIddleState : FriendlyAIState
{
    public override void RunAI(FriendlyAIController controller)
    {
        m_Time += Time.deltaTime;
        controller.IddleLogic();
        if (m_Time > 5f)
        {
            controller.MoveAround();
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
