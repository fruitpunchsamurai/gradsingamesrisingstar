﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Friendly AI State")]
public abstract class FriendlyAIState : ScriptableObject {


    public abstract void RunAI(FriendlyAIController controller);

    public float m_Time = 0f;

    // Use this for initialization
    void Start () {
		
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
