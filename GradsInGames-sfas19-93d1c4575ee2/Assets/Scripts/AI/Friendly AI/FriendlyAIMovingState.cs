﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyAIMovingState : FriendlyAIState
{
    public override void RunAI(FriendlyAIController controller)
    {
        controller.MovementLogic();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
