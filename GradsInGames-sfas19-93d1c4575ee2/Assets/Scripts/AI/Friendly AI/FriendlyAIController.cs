﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum AIStates
{
    iddle,
    moving,
    fighting
}
public class FriendlyAIController : BaseAIController {

    // --------------------------------------------------------------



    // The AI State
    FriendlyAIState m_IddleState;
    FriendlyAIState m_MovingState;
    FriendlyAIState m_FightingState;
    FriendlyAIState m_State;
    // --------------------------------------------------------------

    PlayerController m_PlayerController;
    Transform m_PlayerTransform;
    Vector3 m_Target;
    // --------------------------------------------------------------

    // Use this for initialization
    void Start()
    {
        m_IddleState = ScriptableObject.CreateInstance<FriendlyAIIddleState>();
        m_MovingState = ScriptableObject.CreateInstance<FriendlyAIMovingState>();
        m_FightingState = ScriptableObject.CreateInstance<FriendlyAIFightingState>();
        m_State = m_IddleState;
        // Get Player information
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player)
        {
            m_PlayerController = player.GetComponent<PlayerController>();
            m_PlayerTransform = player.transform;
        }
    }


    private void Update()
    {
        // If the player is dead update the respawn timer and exit update loop
        if (!m_IsAlive)
        {
            return;
        }
        float distanceToPlayer = Vector3.Distance(m_PlayerTransform.position, transform.position);
        if (distanceToPlayer > 5.0f)
        {
            FollowPlayer();
        }
        else
        {
            m_State.RunAI(this);
        }

    }


    public void MovementLogic()
    {
        float distanceToTarget = Vector3.Distance(m_Target, transform.position);
        if (distanceToTarget < 2f)
        {
            m_State = m_IddleState;
        }
    }

    public void IddleLogic()
    {
        m_MovementDirection = Vector3.zero;
        m_CurrentMovementOffset = Vector3.zero;
    }


    void FollowPlayer()
    {
        ApplyDirection(m_PlayerTransform.position);
    }

    public void MoveAround()
    {
        float x = Random.Range(0f, 3f);
        float z = Random.Range(0f, 3f);
        m_Target = m_PlayerTransform.position + new Vector3(x, m_PlayerTransform.position.y, z);
        ApplyDirection(m_Target);
        ChangeAIState(m_MovingState);
    }

    public void ChangeAIState(FriendlyAIState state)
    {
        m_State = state;
        m_State.m_Time = 0f;
    }


}
