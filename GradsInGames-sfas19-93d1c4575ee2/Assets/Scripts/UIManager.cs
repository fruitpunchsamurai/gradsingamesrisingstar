﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    // --------------------------------------------------------------

    [SerializeField]
    Text m_BulletText;

    [SerializeField]
    Text m_GrenadeText;

    [SerializeField]
    Text m_DialogueText;

    [SerializeField]
    Text m_ArcadeEnemiesRemainingText;

    [SerializeField]
    GameObject m_DialogueBox;



    NPCDialogue m_Dialogue;

    HealthBar healthBar;

    public bool talking = false;

    // --------------------------------------------------------------

    private void Start()
    {
        healthBar = GetComponentInChildren<HealthBar>();
        m_DialogueBox.SetActive(false);
    }

    public void TalkToNPC(NPCDialogue dialogue)
    {
        m_Dialogue = dialogue;
        m_DialogueText.text = m_Dialogue.mText;
        talking = true;
        m_DialogueBox.SetActive(true);
    }

    public void SetAmmoText(int bulletCount, int grenadeCount)
    {
        if(m_BulletText)
        {
            m_BulletText.text = "Bullets: " + bulletCount;
        }
        
        if(m_GrenadeText)
        {
            m_GrenadeText.text = "Grenades: " + grenadeCount;
        }
    }

    public void SetHealth(int health)
    {
        healthBar.SetHealth(health);
    }

    public void SetEnemiesRemainingText(int enemyNumber)
    {
        if(m_ArcadeEnemiesRemainingText)
        {
            m_ArcadeEnemiesRemainingText.text = "Enemies: " + enemyNumber;
        }
    }

    public void ManageDialogues()
    {
        m_Dialogue = m_Dialogue.nextDialogue;
        if(m_Dialogue)
        {
            m_DialogueText.text = m_Dialogue.mText;
        }
        else
        {
            talking = false;
            m_DialogueBox.SetActive(false);
        }
    }


}
